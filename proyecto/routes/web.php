<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Rutas inicio */
Route::get('/', function () {
    return view('welcome');
});

Route::get('/inicio', function () {
    return view('welcome');
});

/*Rutas login */
Auth::routes();
Route::get('/logout','Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');

/*Rutas menú  */
Route::resource('agencia','ProductoController');
Route::get('/filtrarCategorias','ProductoController@filtrarCategorias');

/*Rutas menú cesta  */
Route::get('/tienda','CestaController@index');
Route::get('/tienda/vaciar','CestaController@vaciar');
Route::post('tienda','CestaController@store');
Route::get('/tienda/quitar/{id}','CestaController@quitar');
Route::get('/tienda/{id}','CestaController@añadir');
Route::get('/comprar','CestaController@comprar');
Route::get('/presupuesto','CestaController@presupuesto');
Route::get('/factura','CestaController@generarPDF');
Route::get('/presupuestoPDF','CestaController@generarPDFPresupuesto');

/*Rutas menú cesta-pago Tarjeta visa */
Route::resource('/tarjetaVisa','TarjetaVisaController');

/*Rutas menú cesta-pago Tarjeta debito */
Route::resource('/tarjetaDebito','TarjetaDebitoController');

/*Rutas menú cesta-pago Tarjeta visa */
Route::resource('/tarjetaVirtual','TarjetaVirtualController');

/*Rutas menú cesta-pago */
Route::get('/pagar', function () {
    return view('/pagos/pago');
});

/*Rutas menú cesta-pago correos */
Route::get('/email', function () {
    return view('/correos/email');
});

Route::get('/enviado', function () {
    return view('/correos/enviado');
});

/*Rutas menú información covid */
Route::get('/informacion', function () {
    return view('informacionCovid');
});

/*Rutas menú politicas-contactanos */
Route::get('/politicas', function () {
    return view('politicas');
});

Route::get('/contactanos', function () {
    return view('contactanos');
});
