<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
   protected $fillable = ['nombre','apellidos','email','direccion','ciudad','numTarjeta','numExpiracion','numCsv'];
}
