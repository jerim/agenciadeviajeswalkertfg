<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\User;
use PDF;
use Session;

class CestaController extends Controller
{


    public function __construct() {

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');
        ;

        if($cesta == null) {
            $cesta = array();
        }

        if($articulos == null) {
            $articulos = 0;
        }

        $total = 0;

        foreach ($cesta as $key => $producto) {
            $total += $producto->cantidad * $producto->precio;
        }

        return view('tienda.index', ['cesta'=>$cesta,'total'=>$total]);
    }

    public function añadir(Request $request, $id) {
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');

        if($cesta == null) {
            $cesta = array();
            $total = 0;
        }

        $producto = Producto::findOrFail($id);
        $añadir = true;
        $articulos++;

        foreach($cesta as $key=> $elemento){
            if($elemento->id == $producto->id){
                $elemento->cantidad++;
                $añadir= false;
            }
        }

        if($añadir) {
            $producto->cantidad = 1;
            $request->session()->push('cesta',$producto);
        }

        $request->session()->put('articulos',$articulos);

        return redirect('/tienda');

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    public static function contador() {
       $cesta = session()->get('cesta');
       $articulos = 0;

       if($cesta == null) {
          return 0;
       } else {
           foreach($cesta as $producto) {
               $articulos += $producto->cantidad;
           }
       }

       return $articulos;
    }


    public function quitar(Request $request,$id) {
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');

        foreach ($cesta as $key => $producto) {
            if($producto->id == $id) {
                $producto->cantidad--;

                if($producto->cantidad == 0) {
                    unset($cesta[$key]);
                    $request->session()->put('cesta',$cesta);
                }
            }
        }

        $articulos--;
        $request->session()->put('articulos',$articulos);

        return redirect('/tienda');
    }

    public function vaciar(Request $request) {
        $request->session()->forget('cesta');
        $total = 0;

        return redirect('/agencia');
    }

    public function comprar(Request $request){
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');
        ;

        if($cesta == null) {
            $cesta = array();
        }

        if($articulos == null) {
            $articulos = 0;
        }

        $total = 0;

        foreach ($cesta as $key => $producto) {
            $total += $producto->cantidad * $producto->precio;
        }

        return view('tienda.comprar', ['cesta'=>$cesta,'total'=>$total]);
    }

    public function presupuesto() {
        return view('tienda.presupuesto');
    }

    public function generarPDF(Request $request) {
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');
        ;

        if($cesta == null) {
            $cesta = array();
        }

        if($articulos == null) {
            $articulos = 0;
        }

        $total = 0;

        foreach ($cesta as $key => $producto) {
            $total += $producto->cantidad * $producto->precio;
        }

        $data = ['cesta'=>$cesta, 'total'=>$total];

        $pdf = PDF::loadView('tienda.factura',$data);

        return $pdf->download('Factura.pdf');

    }

     public function generarPDFPresupuesto(Request $request) {
        $cesta = $request->session()->get('cesta');
        $articulos = $request->session()->get('articulos');
        ;

        if($cesta == null) {
            $cesta = array();
        }

        if($articulos == null) {
            $articulos = 0;
        }

        $total = 0;

        foreach ($cesta as $key => $producto) {
            $total += $producto->cantidad * $producto->precio;
        }

        $data = ['cesta'=>$cesta, 'total'=>$total];

        $pdf = PDF::loadView('tienda.presupuestoPDF',$data);

        return $pdf->download('Presupuesto.pdf');

    }




}

