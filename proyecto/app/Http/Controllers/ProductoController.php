<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Producto;
use App\Categoria;
use File;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        $productos=Producto::all();
        $categorias=Categoria::all();
        return view('agencia.index',['productos'=>$productos, 'categorias'=>$categorias])->with(['user'=>$productos[0]]); // Le pasa el producto 0 al array.;
    }

    public function filtrarCategorias(Request $request) {
        $categorias=Categoria::all();
        $productos=DB::table('productos')
            ->join('categorias','categorias.id', '=', 'productos.categoria_id')
            ->where('categorias.nombre', '=', $request->categoria)
            ->select('productos.id','productos.nombre','productos.descripcion','productos.origen','productos.numPersona','productos.rutaImg','productos.precio','productos.categoria_id')
            ->get();

        return view('agencia.index',['productos'=>$productos, 'categorias'=>$categorias])->with(['user'=>$productos->first()]);
    }

    public function categorias() {
        return $this->belongsTo('Categoria','id','categoria_id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if(Auth::User() != null) { //Si hay usuario logueado
            $user= User::find(Auth::User()->id); //busco el id del user
            if ($user->can('create', $user)) { //Si es el usuario correcto puesto en la policy puede crear
                return view('agencia.create');
            }
        }
        $productos=Producto::all();
        $categorias=Categoria::all();
        return view('agencia.create',['productos'=>$productos, 'categorias'=>$categorias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'nombre'=>'required|max:255|min:3',
            'descripcion'=>'required|max:255|min:3',
            'origen'=>'required|max:255|min:3',
            'numPersona'=>'required|numeric',
            'precio'=>'required|numeric',
            'file'=>'required|mimes:jpeg,jpg,png',
            'categoria_id'=>'required'
        ];

        $messages = [
            'required'=>'El campo es obligatorio',
            'max'=>'Máximo son 255 caracteres',
            'min'=>'Minimo son 3 caracteres',
            'numeric'=>'Debe ser un campo númerico',
            'mimes'=>'Debe ser jpeg,jpg o png'
        ];

        $request->validate($rules,$messages);

        $entrada=$request->all(); //Almacena el resultado de la consulta entera.

        //Comprueba si esta el archivo(imagen).
        if($archivo = $request->file('file') ) {
            //almacena el nombre de la imagen.
            $nombre=$archivo->getClientOriginalName();

            //Con move hacemos que la ruta de la imagen se mueva a la carpeta viajes.
            $archivo->move('imagenes/productos',$nombre);

            //La ruta debe ser igual al nombre.
            $entrada['rutaImg']=$nombre;
        }

        //Con esto hacemos que se suban a la base de datos.
        Producto::create($entrada);

        return redirect("/agencia");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $producto = Producto::findOrFail($id);
        $categoria = Categoria::findOrFail($producto->categoria_id);
        return view('agencia.show',['producto'=> $producto, 'nombre'=>$categoria->nombre]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $categorias = Categoria::all();
        $producto = Producto::findOrFail($id);
        $this->authorize('update',$producto);
        return view('agencia.edit',['producto'=>$producto, 'categorias'=>$categorias]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = [
           'nombre'=>'required|max:255|min:3',
           'descripcion'=>'required|max:255|min:3',
           'origen'=>'required|max:255|min:3',
           'numPersona'=>'required|numeric',
           'precio'=>'required|numeric',
           'file'=>'required|mimes:jpeg,jpg,png',
           'categoria_id'=>'required'
        ];

       $messages = [
           'required'=>'El campo es obligatorio',
           'max'=>'Máximo son 255 caracteres',
           'min'=>'Minimo son 3 caracteres',
           'numeric'=>'Debe ser un campo númerico',
           'mimes'=>'Debe ser jpeg,jpg o png'
       ];

       $request->validate($rules,$messages);

       $producto= Producto::findOrFail($id);
       $producto->fill($request->all());

         //Comprueba Imagen
         if (isset($_FILES['file'])) {
            $archivo = $request->file('file');
            $nombre = $archivo->getClientOriginalName();
            $archivo->move('imagenes/productos', $nombre);

            //Si existe la imagen.
            if (file_exists(public_path($nombre =  $archivo->getClientOriginalName())))
            {
                unlink(public_path($nombre)); //La elimina
            };
            //Actualiza la imagen
            $producto->rutaImg = $nombre;
        }

        $producto->save();
        return redirect("/agencia");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(Auth::User() != null) { //Si hay usuario logueado
            $user= User::find(Auth::User()->id); //busco el id del user
            if ($user->can('delete', $user)) { //Si es el usuario correcto puesto en la policy puede crear
               return redirect("/agencia");
            }
        }
        $producto = Producto::destroy($id);
        return redirect("/agencia");
    }
}
