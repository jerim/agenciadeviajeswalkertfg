<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pago;

class TarjetaVirtualController extends Controller
{
     public function index() {
        $tarjeta=Pago::all();
        return view('tarjetaVirtual.index',['tarjeta'=>$tarjeta]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $tarjeta= Pago::all();
        return view('tarjetaVirtual.create',['tarjeta'=>$tarjeta]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'nombre' => 'required|max:255|min:3',
            'apellidos' => 'required|max:255|min:3',
            'email' => 'required|max:255|min:3',
            'direccion' =>  'required|max:255|min:3',
            'ciudad' => 'required|max:255|min:3',
            'numTarjeta'=>'required|numeric',
            'numExpiracion'=>'required|numeric',
            'numCsv'=>'required|numeric',
        ];

        $messages= [
            'required'=>'Los campos son obligatorios',
            'max'=>'Máximo 255 caracteres.',
            'min'=>'Minimo debe ser tres números',
            'numeric'=>'Debe ser un caracter númerico',
        ];

        $request->validate($rules,$messages);

        $tarjeta = new Pago();
        $tarjeta->fill($request->all());
        $tarjeta->save();
        return redirect("/tarjetaVirtual");
    }


    public function destroy($id) {
        $tarjeta = Pago::destroy($id);
        return redirect("/tienda");
    }


}
