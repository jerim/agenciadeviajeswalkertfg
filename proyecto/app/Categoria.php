<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = ['id','nombre'];

    public static function dptProductos(){
        return $this->hasMany('App\Producto','categoria_id');
    }

}
