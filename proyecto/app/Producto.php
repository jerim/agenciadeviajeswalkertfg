<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    protected $fillable = ['nombre','descripcion','origen','numPersona','rutaImg','precio','categoria_id'];

    public function categoria(){
        return $this->hasOne(Categoria::class);
    }
}

