<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([ //1
            'nombre'=>'Viajes',
        ]);

        DB::table('categorias')->insert([ //2
            'nombre'=>'Excursiones',
        ]);

        DB::table('categorias')->insert([ //3
            'nombre'=>'Circuitos',
        ]);

        DB::table('categorias')->insert([ //4
            'nombre'=>'Ofertas',
        ]);

        DB::table('categorias')->insert([ //5
            'nombre'=>'Hoteles',
        ]);
    }
}
