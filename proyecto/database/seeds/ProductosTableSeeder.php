<?php

use Illuminate\Database\Seeder;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'id'=> 2,
            'nombre'=>'Visita gastronómica y santuario de Gyeonggijeon (8h)',
            'descripcion'=> "Jeonju es uno de los destinos más populares de
            Corea del sur para los viajeros por su arquiquectura histórica y su exquisito paronama culinario. Es la cuna del bibimbap y del santuario de gyeonggijeon donde se pueden ver los distintos retatros de los reyes de corea.
            Incluye la entrada al santuario, el guía y comida en el restaurante",
            'origen'=>'Jeonju(Corea del sur)',
            'numPersona'=>1,
            'rutaImg'=>"jeonju.jpg",
            'precio'=>48,
            'categoria_id'=> 2 //excursión
        ]);

        DB::table('productos')->insert([
            'id'=> 3,
            'nombre'=>'Viaje en burro y visita en Fira (5h)',
            'descripcion'=> "El burro es el negocio más antiguo de la isla como carga o medio de transporte para subir y bajar los quinientos ochenta peldaños solos sin guia. Y la ciudad de Fira donde es un lugar tranquilo ideal para pasear e ir de tiendas. Incluye la travesia del burro, el guía y almuerzo en restaurante",
            'origen'=>'Sartorini(Grecia)',
            'numPersona'=>1,
            'rutaImg'=>"sartorini.JPG",
            'precio'=>28,
            'categoria_id'=> 2 //excursión
        ]);

        DB::table('productos')->insert([
            'id'=> 4,
            'nombre'=>'Los famosos molinos de Mykonos (3h)',
            'descripcion'=> "Mykonos es una pequeña isla de Grecia perteneciente al archipielago de las islas cicladas. Alli sopla el viento sin cesar durante los doscientos-trescientos dias al año por eso los molinos son el simbolo de Mykonos. Incluye el guía",
            'origen'=>'Mykonos(Grecia)',
            'numPersona'=>1,
            'rutaImg'=>"molinos.JPG",
            'precio'=>12,
            'categoria_id'=> 2 //excursión
        ]);

        DB::table('productos')->insert([
            'id'=> 5,
            'nombre'=>'Visita a Pompeya (6h)',
            'descripcion'=> "Camina por la antigua cuidad de Pompeya, conociendo como era la vida alli, sus costumbres y creencias antes de ser arrastada porel volcán Vesubio. Incluye la entrada a Pompeya, el transporte y el guía.",
            'origen'=>'Nápoles(Italia)',
            'numPersona'=>1,
            'rutaImg'=>"pompeya.jpg",
            'precio'=>50,
            'categoria_id'=> 2 //excursión
        ]);

        DB::table('productos')->insert([
            'id'=> 6,
            'nombre'=>'Joyas de Italia',
            'descripcion'=> "Descubre las maravillas de las ciudades de Roma, Venecia, Florencia y Genóva. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con el desayuno, los traslados entre las ciudades, las entradas a los museos, el coliseo, las iglesias más famosas, el palacio ducal de Venecia e incluye guía.",
            'origen'=>'Italia',
            'numPersona'=>1,
            'rutaImg'=>"venecia.JPG",
            'precio'=>900,
            'categoria_id'=> 3 //circuitos
        ]);

        DB::table('productos')->insert([
            'id'=> 7,
            'nombre'=>'Capitales balticas',
            'descripcion'=> "Descubre las maravillas de las ciudades del norte, su historia  y cultura de Copenhague, Tallin, San petesburgo y AArhus. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con media pensión, los traslados entre las ciudades, las entradas a los museos, las iglesias más famosas e incluye guía.",
            'origen'=>'Italia',
            'numPersona'=>2,
            'rutaImg'=>"tallin.JPG",
            'precio'=>1500,
            'categoria_id'=> 3 //circuitos
        ]);

        DB::table('productos')->insert([
            'id'=> 8,
            'nombre'=>'El secreto de Corea',
            'descripcion'=> "Descubre las maravillas de las ciudades asiaticas, su gastronómia y cultura de Seoul, Jeonju, Busan y Suwon. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con media pensión, los traslados entre las ciudades, las entradas a los museos, las iglesias budistas más famosas, el palacio real gyeongbokgung de seoul e incluye guía.",
            'origen'=>'Corea del sur',
            'numPersona'=>1,
            'rutaImg'=>"seoul.jpg",
            'precio'=>3999,
            'categoria_id'=> 3 //circuitos
        ]);

        DB::table('productos')->insert([
            'id'=> 9,
            'nombre'=>'Un susurro del mediterraneo',
            'descripcion'=> "Descubre las maravillas de las ciudades mediterraneas, sus paisajes y gastronómia de Monaco, MonteCarlo, Mykonos, Sartorini y Atenas. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con media pensión, los traslados entre las ciudades, las entradas a los museos, las iglesias  más famosas, el palacio real de monaco e incluye guía.",
            'origen'=>'Corea del sur',
            'numPersona'=>1,
            'rutaImg'=>"casino.JPG",
            'precio'=>600,
            'categoria_id'=> 3//circuitos
        ]);

        DB::table('productos')->insert([
            'id'=> 10,
            'nombre'=>'Un susurro del mediterraneo',
            'descripcion'=> "Descubre las maravillas de las ciudades mediterraneas, sus paisajes y gastronómia de Monaco, MonteCarlo, Mykonos, Sartorini y Atenas. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con media pensión, los traslados entre las ciudades, las entradas a los museos, las iglesias  más famosas, el palacio real de monaco e incluye guía.",
            'origen'=>'Corea del sur',
            'numPersona'=>1,
            'rutaImg'=>"casino.JPG",
            'precio'=>300,
            'categoria_id'=> 4 //oferta
        ]);

        DB::table('productos')->insert([
            'id'=> 11,
            'nombre'=>'Visita gastronómica y santuario de Gyeonggijeon (8h)',
            'descripcion'=> "Jeonju es uno de los destinos más populares de
            Corea del sur para los viajeros por su arquiquectura histórica y su exquisito paronama culinario. Es la cuna del bibimbap y del santuario de gyeonggijeon donde se pueden ver los distintos retatros de los reyes de corea.
            Incluye la entrada al santuario, el guía y comida en el restaurante",
            'origen'=>'Jeonju(Corea del sur)',
            'numPersona'=>1,
            'rutaImg'=>"jeonju.jpg",
            'precio'=>18,
            'categoria_id'=> 4 //oferta
        ]);

        DB::table('productos')->insert([
            'id'=> 12,
            'nombre'=>'Joyas de Italia',
            'descripcion'=> "Descubre las maravillas de las ciudades de Roma, Venecia, Florencia y Genóva. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con el desayuno, los traslados entre las ciudades, las entradas a los museos, el coliseo, las iglesias más famosas, el palacio ducal de Venecia e incluye guía.",
            'origen'=>'Italia',
            'numPersona'=>1,
            'rutaImg'=>"venecia.JPG",
            'precio'=>250,
            'categoria_id'=> 4 //oferta
        ]);

        DB::table('productos')->insert([
            'id'=> 13,
            'nombre'=>'Capitales balticas',
            'descripcion'=> "Descubre las maravillas de las ciudades del norte, su historia  y cultura de Copenhague, Tallin, San petesburgo y AArhus. Incluye los vuelos, el alojamiento en hoteles de cuatro estrellas con media pensión, los traslados entre las ciudades, las entradas a los museos, las iglesias más famosas e incluye guía.",
            'origen'=>'Italia',
            'numPersona'=>2,
            'rutaImg'=>"tallin.JPG",
            'precio'=>500,
            'categoria_id'=> 4 //oferta
        ]);

        DB::table('productos')->insert([
            'id'=> 14,
            'nombre'=>'El hotel Hermes',
            'descripcion'=> "El hotel Hermes situado en el centro de atenas a diez minutos del panteon. El hotel tiene mucha luz natural, decoración moderna y varias habitaciones. El restaurante del hotel permanece abierto los siete días de la semana y sirve platos tipicos griegos.",
            'origen'=>'Grecia',
            'numPersona'=>1,
            'rutaImg'=>"hermes.jpg",
            'precio'=>50,
            'categoria_id'=> 5 //hoteles
        ]);

        DB::table('productos')->insert([
            'id'=> 15,
            'nombre'=>'El hotel Paradise',
            'descripcion'=> "El hotel Paradise situado a las afueras de copenhague a media hora de la ciudad. El hotel tiene mucha luz natural, decoración moderna y varias habitaciones. El restaurante del hotel permanece abierto los cinco días de la semana y sirve platos tipicos y de distintos paises.",
            'origen'=>'Dinamarca',
            'numPersona'=>1,
            'rutaImg'=>"paradise.jpg",
            'precio'=>70,
            'categoria_id'=> 5 //hoteles
        ]);

        DB::table('productos')->insert([
            'id'=> 16,
            'nombre'=>'El hotel luxury hall',
            'descripcion'=> "El hotel luxury hall situado en el centro de monaco a cinco minutos del centro. El hotel tiene mucha luz natural, decoración moderna y varias habitaciones. El restaurante del hotel permanece abierto los siete días de la semana y sirve platos tipicos y de distintos paises.",
            'origen'=>'Monaco',
            'numPersona'=>1,
            'rutaImg'=>"hall.jpg",
            'precio'=>3000,
            'categoria_id'=> 5 //hoteles
        ]);

        DB::table('productos')->insert([
            'id'=> 17,
            'nombre'=>'El hotel the fall angel',
            'descripcion'=> "El hotel the fall angel situado a las afueras de San petesburgo a una hora de la ciudad. El hotel tiene mucha luz natural, decoración moderna y varias habitaciones. El restaurante del hotel permanece abierto los siete días de la semana y sirve platos tipicos.",
            'origen'=>'Rusia',
            'numPersona'=>1,
            'rutaImg'=>"fall.jpg",
            'precio'=>1500,
            'categoria_id'=> 5 //hoteles
        ]);

        DB::table('productos')->insert([
            'id'=> 18,
            'nombre'=>'Busan',
            'descripcion'=> "Busan una importante ciudad maritima de corea del sur y una de las más grandes ciudades maritimas del mundo. Parte de su atractivo gira alrededor del mar y los lugares con intereses culturales.",
            'origen'=>'Corea del sur',
            'numPersona'=>2,
            'rutaImg'=>"busan.jpg",
            'precio'=>2000,
            'categoria_id'=> 1 //viajes
        ]);

        DB::table('productos')->insert([
            'id'=> 19,
            'nombre'=>'Copenhague',
            'descripcion'=> "Copenhague considerada una de las mejores ciudades del mundo para vivir, con su interesante historia y filosofia.",
            'origen'=>'Dinamarca',
            'numPersona'=>2,
            'rutaImg'=>"copenhague.jpg",
            'precio'=>800,
            'categoria_id'=> 1 //viajes
        ]);

        DB::table('productos')->insert([
            'id'=> 20,
            'nombre'=>'El verdadero protector de la ciudad',
            'descripcion'=> "Un verdadero dilema abarca en esta ciudad, unos dicen que fueron los soldados quienes defendieron la ciudad. Otros dicen que fueron los fantasmas de Lokrum..¿Quién es su protector?. Un día el alcalde contrata a un grupo de expertos para acabar con este dilema que amarga a los habitantes de Dubrovnik. Este viaje de seis días y cinco noches exploraremos la ciudad en busca de pistas para resolver el dilema y os acompañara un guía por la ciudad.",
            'origen'=>'Dubrovnik(Croacia)',
            'numPersona'=>1,
            'rutaImg'=>"dubrovnik.JPG",
            'precio'=>3500,
            'categoria_id'=> 1 //viajes
        ]);

        DB::table('productos')->insert([
            'id'=> 21,
            'nombre'=>'San petesburgo',
            'descripcion'=> "San petesburgo es la segunda ciudad más importante de toda rusia, apodada la venecia del norte. Una maravilla tanto a nivel histórico como cultural, un destino perfecto para descansar, explorar y para eventos especiales.",
            'origen'=>'Dubrovnik(Croacia)',
            'numPersona'=>1,
            'rutaImg'=>"sanpetesburgo.JPG",
            'precio'=>3500,
            'categoria_id'=> 1 //viajes
        ]);

    }
}
