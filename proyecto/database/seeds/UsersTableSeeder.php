<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre'=>'Carmen',
            'email'=>'agenteaw@gmail.com',
            'password'=>bcrypt('secret'),
            'role_id'=> 2,
        ]);

        DB::table('users')->insert([
            'nombre'=>'Andy',
            'email'=>'administrador@gmail.com',
            'password'=>bcrypt('secret'),
            'role_id'=> 3,
        ]);
    }
}
