<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('productos');
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('origen');
            $table->integer('numPersona');
            $table->string('rutaImg');
            $table->double('precio');

       // $table->unsignedInteger('categoria_id')->nullable();
       // $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::table('productos',function (Blueprint $table){
            $table->foreignId('categoria_id')->on('categorias')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('productos');
         Schema::table('productos', function (Blueprint $table) {
            $table->dropForeign('categoria_id');
        });

            Schema::dropIfExists('productos');
    }
}
