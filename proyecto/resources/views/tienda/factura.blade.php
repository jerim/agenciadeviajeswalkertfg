<!DOCTYPE html>
<html>
<head>
    <title>Factura agencia de viajes walker</title>
    <style type="text/css">
        table {
          width:100%;
          border-collapse: collapse;
        }
        table, td,th { border: 1px solid black; }
    </style>
</head>
<body>

<h1>Factura agencia de viajes walker</h1>
<hr>
<h3>Datos de los productos</h3>

<table>
    <thead>
        <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Precio</th>
        </tr>
    </thead>

    <tbody>
        @forelse($cesta as $producto)
           <tr>
              <td>{{$producto->nombre}}</td>
              <td>{{$producto->cantidad}}</td>
              <td>{{$producto->precio}} €</td>
           </tr>

        @empty
           <tr>
             <td colspan="5">¡No hay productos en la cesta!</td>
           </tr>
        @endforelse
    </tbody>

    <tfoot>
        <tr>
            <td colspan="3" class="elemento">Total a pagar: <strong>{{$total}} €</strong></td>
        </tr>
    </tfoot>
</table>

</body>
</html>
