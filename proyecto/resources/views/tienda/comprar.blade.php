@extends('layouts.app')
@section('content')
 <div class="row">
    <div class="col-sm-6">
        <div class="card">
          <div class="card-header" style="background-color: #3b83bd; color:white;"> Productos a pagar  </div>
              <div class="card-body">
                @forelse($cesta as $producto)
                    <ul class="list-group">
                          <li class="list-group-item">{{$producto->nombre}} - {{$producto->precio}} € </li>
                    </ul>
                    @empty
                        <p class="text">¡No hay nada para pagar!</p>
                    @endforelse
                </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-header" style="background-color: #3b83bd; color:white;">Opciones de pago</div>
               <div class="list-group">
                  <a href="/tarjetaVisa/create" class="list-group-item list-group-item-action">Tarjeta de visa</a>
                  <a href="/tarjetaDebito/create" class="list-group-item list-group-item-action">Tarjeta de debito</a>
                  <a href="/tarjetaVirtual/create" class="list-group-item list-group-item-action">Tarjeta virtual</a>
                </div>
        </div>
    </div>

</div>
@endsection
