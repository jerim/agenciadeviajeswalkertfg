@extends('layouts.app')

@section('content')
  <div class="card text-center">
      <div class="card-header" style="background-color: #333; color:white;">Presupuesto</div>
      <div class="card-body">
        <p class="card-text">Usted puede ver el presupuesto en pdf o enviarlo por email.</p>
        <a href="/presupuestoPDF" class="btn btn-primary">Presupuesto PDF</a>
        <a href="/email" class="btn btn-primary">Envio por email</a>
      </div>
      <div class="card-footer text-muted" style="background-color: #333; color:white;"><a href="/agencia" class="btn" style="background-color: #3b83bd; color:white;">Volver a descubre</a></div>
</div>
@endsection
