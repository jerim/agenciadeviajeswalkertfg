@extends('layouts.content')

@section('content')

<div class="jumbotron text-center hoverable p-4" style="background-color:white;">
  <div class="row">
    <div class="col-md-4 offset-md-1 mx-3 my-3">
      <div class="view overlay">
        <img src="../imagenes/error/error403.jpg" class="img-fluid" alt="Sample image for first version of blog listing">
      </div>

    </div>

    <div class="col-md-7 text-md-left ml-3 mt-3">
      <h4 class="h4 mb-4">Lo sentimos, usted no está autorizado.</h4>
      <a href="/agencia" class="btn" style="background-color: #86B22C; color:white;">Volver a inicio</a>

    </div>
  </div>
</div>
@endsection
