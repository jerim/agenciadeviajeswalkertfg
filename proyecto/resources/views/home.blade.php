@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card card-inverse" style="background-color: #333; border-color:#333;">
            <div class="card-block">
                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <h2 class="card-title" style="color:white;">Bienvenido {{ucfirst(Auth()->user()->nombre)}}</h2>
                        <p class="card-text" style="color:white;">
                           @if (session('status'))
                               <div class="alert alert-success"role="alert">
                                  {{ session('status') }}
                               </div>
                           @endif

                         Estas logueado!
                        </p>

                    </div>

                    <div class="col-md-4 col-sm-4 text-center">
                        <img class="btn-md" src="../imagenes/perfil/avatar.png" style="border-radius: 50%; width:35%; margin-top: 4%;">
                    </div>

                    <div class="col-md-4 col-sm-4 text-left">
                        <a href="/logout" class="btn" style="background-color: #86B22C; color:white;" >Cerrar sesión</a>
                        <a href="/inicio" class="btn" style="background-color: #86B22C; color:white;">Inicio</a>
                    </div>
                </div>
            </div>

            </div>
        </div>

         <div class="col-12">
            <div class="card card-inverse" style="background-color: #333; border-color:#333; margin-top: 2%;">
                <div class="card-block">
                   <div class="row">
                       <div class="col-md-8 col-sm-8">
                          <h2 class="card-title" style="color:white;">Aviso de cuenta</h2>
                          <p class="card-text" style="color:white;">
                            La cuenta del usuario, está sujeta a la página web agencia de viajes walker y a sus elementos correspondientes.
                            Si usted hace algo malicioso,fradulento o ilegal con la cuenta.
                            Se le procedera a la eliminación inmediata de la cuenta del usuario.
                          </p>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

