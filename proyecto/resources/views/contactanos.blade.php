@extends('layouts.app')

@section('content')

<div class="card mb-3" style="background-color: #3b83bd; color:white;">
<h5 class="card-header">Información</h5>
 <div class="card-body">
    <p class="card-text">Bienvenidos a la agencia de viajes Walker.
     Aqui encontrarán las dos maneras de poder contactarnos a través de la página web de la agencia.
    </p>

    <p class="card-text">
     De esa manera si tienes dudas sobre algún viaje, quieres algún tipo de información concreto de algunos de nuestros servicios de la página web, saber los precios de los transportes o saber si algún hotel tiene habitaciones libre, si hay alguna plaza disponible en alguna excursión o circuito que desees hacer, comentar tu experiencia tras haber hecho un viaje con nosotros  o deseas contactar con algún agente de la agencia.
    </p>

    <p class="card-text">
     Aqui debajo dejamos la información de contacto.
     No dudes en contactarnos, estaremos encantados de contestarte.
    </p>

    <p class="card-text">
     Disfruta e elige el viaje de tus sueños.</p>
    </p>

    </div>
</div>

<div class="card mb-3" style="background-color: #3b83bd; color:white;">
    <h5 class="card-header">Contactanos</h5>
        <div class="card-body">
          <p class="card-text">Aqui están las dos formas donde puedes contactar con nosotros. </p>
          <p class="card-text">Email : agenciadeviajeswalker@gmail.com</p>
          <p class="card-text">Teléfono :(0034) 9765584125  </p>
        </div>
</div>


<div class="card mb-3" style="background-color: #3b83bd; color:white;">
    <h5 class="card-header">Redes sociales</h5>
        <div class="card-body">
            <p class="card-text">Aqui están nuestras redes sociales para que nos sigas. </p>
            <p class="card-text"> <img src="https://img.icons8.com/color/40/000000/facebook.png"/><img src="https://img.icons8.com/color/40/000000/instagram-new.png"/></p >
        </div>
</div>

@endsection
