@extends('layouts.content')
@section('content')

<div class="jumbotron text-center hoverable p-4" style="background-color:white;">
  <div class="row">
    <div class="col-md-4 offset-md-1 mx-3 my-3">
      <div class="view overlay">
        <img src="/imagenes/productos/{{$producto->rutaImg}}" class="img-fluid">
      </div>

    </div>

    <div class="col-md-7 text-md-left ml-3 mt-3">
      <h4 class="h4 mb-4">{{$producto->nombre}} desde {{$producto->precio}} €</h4>
       <p class="text">{{$producto->descripcion}}</p>
        <p class="text">Lo que incluye en el viaje es el vuelo más el transporte desde el aeropueto al hotel.</p>
        <p class="text">Lo que no incluye en el viaje son los restaurantes ,tiendas ni las entradas para los museos y monumentos históricos.
        Si lo desean pueden obtenerlos en alguna excursión de la agencia.</p>

      <a href="/tienda/{{$producto->id}}" class="btn" style="background-color: #3b83bd; color:white;">Guardar  en cesta</a>
      <a href="/agencia" class="btn" style="background-color: #3b83bd; color:white;">Volver a inicio</a>

    </div>
  </div>
</div>

@endsection
