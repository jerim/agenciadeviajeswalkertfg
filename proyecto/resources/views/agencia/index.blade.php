@extends('layouts.app')

@section('content')

@can('create',$user)
<a href="/agencia/create" class="btn btn-success" style="margin-left: 10px; ">Crear producto</a>
@endcan

<div class="mb-2" style="margin-top: 10px;margin-left: 10px; ">
    <form action="/filtrarCategorias" method="GET" class="form-inline">
        <label for="category_filter">Filtrar por:</label>
        <select name="categoria" id="categoriaFilter" class="form-control">
            <option value="">Selecciona</option>
            @if(count($categorias))
               @foreach($categorias as $categoria)
                   <option value="{{$categoria->nombre}}" >{{$categoria->nombre}}</option>
               @endforeach
            @endif
        </select>
        <button type="submit" class="btn" name="Buscar" value="Buscar" class="btn" style="background-color: #3b83bd; color:white;">Buscar</button>
    </form>
</div>



<div class="card-deck" style="margin-top:5px; margin-right: 1px">
@foreach($productos as $producto)
    <div class="col mb-2">
        <div class="card h-100" style="width:270px">
            <img src="imagenes/productos/{{$producto->rutaImg}}" class="img-thumbnail" height="auto" />
                <div class="card-body">
                    <h5 class="card-title">{{$producto->nombre}} desde {{$producto->precio}} € </h5>
                     <p class="card-text">Categoria:{{$categorias[$producto->categoria_id-1]->nombre}}</p>
                    <a href="/agencia/{{$producto->id}}" class="btn btn-success">Información Viaje</a>
                    @can('update',$producto)
                    <a href="/agencia/{{$producto->id}}/edit" class="btn btn-primary">Modificar Viaje</a>
                    @endcan
                    @can('delete',$producto)
                    <form method="post" action="/agencia/{{$producto->id}}">
                      {{csrf_field()}}
                        <input type="hidden" name="_method" value="delete">
                        <input type="submit" class="btn btn-danger" name="Eliminar" value="Eliminar">
                    </form>
                    @endcan
                </div>
        </div>
    </div>
@endforeach
</div>
@endsection
