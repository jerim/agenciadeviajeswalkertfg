@extends('layouts.app')

@section('content')

<div class="card mb-3" style="background-color: #3b83bd; color:white;">
  <h5 class="card-header">Información del Covid-19 en la agencia de viajes Walker</h5>
    <div class="card-body">
        <p class="card-text">
         En estos nuevos tiempos, todo ha cambiado trastocando nuestras formas de vida.
         En agencia de viajes Walker se sigue adaptando para poder ofrecer todos los
         servicios con toda la máxima seguridad.
        </p>
    </div>
</div>
<div class="card mb-3" style="background-color: #3b83bd; color:white;">
    <h5 class="card-header">Medidas covid-19 en hoteles</h5>
        <div class="card-body">
            <p class="card-text">
             Limpieza: Protocolo de limpieza y desinfección en habitaciones, zonas comunes etc.
             Eliminación de elementos decorativos y articulos.
            </p>
            <p class="card-text">
             Desinfección: Alfombras desinfectantes, gel hidroalcóhilico.
            </p>
            <p class="card-text">
             Distanciamento social: Marcas en las zonas comunes para garantizar la distancia social.
            </p>
            <p class="card-text">
             Restaurantes:Limitación de aforo, mayor distancia entre mesas y ampliación de horarios.
            </p>
        </div>
</div>

<div class="card mb-3" style="background-color: #3b83bd; color:white;">
    <h5 class="card-header">Medidas covid-19 en transportes</h5>
        <div class="card-body">
            <p class="card-text">
             Limpieza y desinfección: Protocolo de limpieza y desinfección en los mostradores, zonas de baños,superficies etc.
             Sistema de ventilación se renueva cada pocos minutos, eliminando los virus y bacterias del aire.
            </p>
            <p class="card-text">
             Distanciamento social: Instalación de mamparas en los mostradores, atención al cliente.
             Las mascarillas es de uso obligatorio en todo momento menos cuando se coma o beba.
            </p>
            <p class="card-text">
             Protocolo de salud :Se hará un control de salud certificando si usted tiene sintómas.
             Además para poder subir al transporte es obligatio presentar una PCR negativa.
            </p>
        </div>
</div>


<div class="card mb-3" style="background-color: #3b83bd; color:white;">
    <h5 class="card-header">Medidas covid-19 en viajes y circuitos</h5>
        <div class="card-body">
            <p class="card-text">
             Protocolo de salud :Se hará un control de salud certificando si usted tiene sintómas.
             Además para poder subir al transporte es obligatio presentar una PCR negativa.
            </p>
            <p class="card-text">
             Excursiones: Visitas en las escalas seguras, al aire libre.
             Puede con llevar a modificaciones en los intinerarios de las excursiones.
             En los medios de transporte serán desisfenctados antes y después de su uso.
             El uso de la mascarillas es de uso obligatorio en todo momento menos cuando se coma o beba.
             Y el uso del material usado en las excursiones será desisfenctados antes y después de su uso.
            </p>
            <p class="card-text">
             Protocolo médico de seguro: El personal médico está entrenado especialmente para tratar a pacientes
             con covid-19, equipados con test de prueba y alojamientos especializados.
            </p>
        </div>
</div>
@endsection

