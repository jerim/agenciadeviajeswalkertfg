@extends('layouts.content')
@section('content')
 <div class="row" style="margin-left:-10px; margin-top: 25px;">
    <div class=" col-sm-6 col-md-12" >
        <div class="card">
            <div class="card-header">Rellena los datos para realizar el pago con la tarjeta visa</div>
                <form method="POST" action="/tarjetaVisa" enctype="multipart/form-data">
                      {{csrf_field()}}
                    <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nombre">Nombre</label>
                                <input type="text" name="nombre" class="form-control" value="{{old('nombre')}}">
                                @if($errors->first('nombre'))
                                    <div class="alert alert-danger">{{$errors->first('nombre')}}</div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="apellidos">Apellidos</label>
                                <input type="text" name="apellidos" class="form-control"value="{{old('apellidos')}}">
                                @if($errors->first('apellidos'))
                                    <div class="alert alert-danger">{{$errors->first('apellidos')}}</div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="text" name="email" class="form-control" value="{{old('email')}}" >
                                @if($errors->first('email'))
                                    <div class="alert alert-danger">{{$errors->first('email')}}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="direccion">Direccion</label>
                                <input type="text" name="direccion" class="form-control"value="{{old('direccion')}}">
                                @if($errors->first('direccion'))
                                    <div class="alert alert-danger">{{$errors->first('direccion')}}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="ciudad">Ciudad</label>
                                <input type="text"  name="ciudad" class="form-control"  value="{{old('ciudad')}}">
                                @if($errors->first('ciudad'))
                                    <div class="alert alert-danger">{{$errors->first('ciudad')}}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="numTarjeta">Número de la tarjeta</label>
                                <input type="Integer" name="numTarjeta" class="form-control" value="{{old('numTarjeta')}}">
                                @if($errors->first('numTarjeta'))
                                    <div class="alert alert-danger">{{$errors->first('numTarjeta')}}</div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <label for="numExpiracion">Expiracion</label>
                                <input type="Integer" name="numExpiracion" class="form-control" value="{{old('numExpiracion')}}">
                                @if($errors->first('numExpiracion'))
                                    <div class="alert alert-danger">{{$errors->first('numExpiracion')}}</div>
                                @endif
                            </div>

                            <div class="form-group col-md-6">
                                <label for="numCsv">CVV</label>
                                <input type="Integer" name="numCsv" class="form-control" value="{{old('numCsv')}}">
                                 @if($errors->first('numCsv'))
                                    <div class="alert alert-danger">{{$errors->first('numCsv')}}</div>
                                @endif
                            </div>
                        </div>

                        <input type="submit" class="btn btn-success" name="Pagar" value="Pagar">
                         <a href="/inicio" class="btn btn-success">Volver al inicio</a>
                </form>
        </div>
    </div>
</div>
@endsection
