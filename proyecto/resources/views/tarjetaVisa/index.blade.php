@extends('layouts.app')

@section('content')
<div class="row">
      @foreach($tarjeta as $tarjeta)
  <div class="col-sm-6">
        <div class="card">
          <div class="card-header" style="background-color: #3b83bd; color:white;"> Datos tarjeta:  </div>
            <div class="card-body">
                <p class="card-text">Nombre: {{$tarjeta->nombre}}</p>
                <p class="card-text">Apellido: {{$tarjeta->apellidos}}</p>
                <p class="card-text">Email: {{$tarjeta->email}}</p>
                <p class="card-text">Dirección: {{$tarjeta->direccion}}</p>
                <p class="card-text">Ciudad: {{$tarjeta->ciudad}}</p>
                <p class="card-text">Número de la Tarjeta: {{$tarjeta->numTarjeta}}</p>
                <p class="card-text">Número de expiración: {{$tarjeta->numExpiracion}}</p>
                <p class="card-text">CSV: {{$tarjeta->numCsv}}</p>
            </div>

    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-header" style="background-color: #3b83bd; color:white;"> Opciones:</div>
        <div class="card-body">
          <p class="card-text"><a href="/pagar" class="btn" style="background-color: #3b83bd; color:white;">Pagar</a></p>
          <form method="post" action="/tarjetaVisa/{{$tarjeta->id}}">
              {{csrf_field()}}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" class="btn btn-danger" name="Cancelar" value="Cancelar ">
          </form>
        </div>
    </div>
  </div>
  @endforeach
</div>

@endsection
