@extends('layouts.app')

@section('content')
<div class="container">
     <form class="text-center border border-light p-5" action="{{ route('register') }}" method="POST">
       @csrf
       <p class="h4 mb-4">Registrate</p>
        <!--Nombre-->
           <input id="nombre" type="text" class="form-control mb-4 @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus placeholder="Nombre">
            @error('nombre')
                <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                </span>
            @enderror

        <!--Email-->
            <input id="email" type="email" class="form-control mb-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                </span>
            @enderror

        <!--Contraseña-->
            <input id="password" type="password" class="form-control mb-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

        <!--Confirma contraseña-->
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirma contraseña">

        <!--Registrate-->
            <button class="btn btn-block my-2" type="submit" style="background-color: #3b83bd; color:white;">Registrate</button>

     </form>
</div>
@endsection
