@extends('layouts.app')

@section('content')
<div class="container">
      <form class="text-center border border-light p-5" method="POST" action="{{ route('login') }}">
        @csrf
        <p class="h4 mb-4">Inicia sesión</p>
        <!--Email-->
        <input type="email" class="form-control mb-4" @error('email') is-invalid @enderror" name="email" value="{{ ('email') }}" required autocomplete="email" autofocus name="email" placeholder="Correo electronico">
              @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror


         <!--Contraseña-->
         <input type="password" class="form-control mb-4" @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" name="password" placeholder="Contraseña">
              @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror


        <div class="d-flex justify-content-around">
            <div>
                <!--Recuerdame-->
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="defaultLoginFormRemember" name="remember" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="defaultLoginFormRemember">Recuerdame</label>
                </div>
            </div>

            <div>
                <!--Contraseña olvidada-->
                  @if (Route::has('password.request'))
                      <a  href="{{ route('password.request') }}">¿Contraseña olvidada?<a>
                  @endif
            </div>
        </div>

      <!--Inicia sessión-->
        <button class="btn btn-block my-2" type="submit" style="background-color: #3b83bd; color:white;">Inicia sesión</button>

      <!--Registrate-->
      <p>¿No es miembro?
          <a href="{{ route('register') }}">Registrate</a>
      </p>


      </form>
</div>
@endsection
