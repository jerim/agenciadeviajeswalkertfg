@extends('layouts.app')

@section('content')
  <div class="card text-center">
      <div class="card-header" style="background-color: #333; color:white;">Envio de email</div>
      <div class="card-body">
        <h5 class="card-title">El email ha sido enviado.</h5>
        <p class="card-text">En los proximos días le llegara un email con la factura.</p>
      </div>
      <div class="card-footer text-muted" style="background-color: #333; color:white;"><a href="/agencia" class="btn" style="background-color: #3b83bd; color:white;">Volver al inicio</a></div>
</div>
@endsection
