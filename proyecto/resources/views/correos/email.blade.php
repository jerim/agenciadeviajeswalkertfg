@extends('layouts.app')

@section('content')
  <div class="card text-center">
      <div class="card-header" style="background-color: #333; color:white;">Envio de email</div>
      <div class="card-body">
        <h5 class="card-title">Introduzca el email.</h5>
        <form>
            <div class="form-group row">
                <label for="email">Email</label>
                <input type="email" id="exampleInputEmail1" name="email" class="form-control">
            </div>
                <a href="/enviado" class="btn btn-success" type="submit">Enviar</a>
        </form>
      </div>
      <div class="card-footer text-muted" style="background-color: #333; color:white;"><a href="/tienda" class="btn" style="background-color: #3b83bd; color:white;">Volver a descubre</a></div>
</div>
@endsection
